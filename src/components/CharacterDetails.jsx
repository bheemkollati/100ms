import React,{useState, useEffect} from 'react'
import './CharacterDetails.scss';
import {Link} from 'react-router-dom';
import axios from 'axios';

function CharacterDetails(props) {

    const [quotes,setQuotes]=useState([]);
    const [loader,setLoader]=useState(false);
    const [char,setChar]=useState("")
    useEffect(()=>{
        console.log(props);

        console.log(props.match.params.id);
        setLoader(true);
        axios.get(`https://breakingbadapi.com/api/characters/${props.match.params.id}`)
            .then(character => {
                setChar(character.data[0]);
                axios.get(`https://breakingbadapi.com/api/quote?author=${character.data[0].name}`)
                .then(res =>{console.log(res.data);setQuotes(res.data);setLoader(false);})
                .catch(err => console.log(err));
            })
            .catch(error => console.log(error));
        
        
         
    
      },[])
    return (
        <div className="characterwrapper">
            <img src={`${char.img}`} alt="" />
            <div className="char">
                <div>Name : {char.name}</div>
                <div>Birtyday :{char.birthday}</div>
                <div>Occupation : {char.occupation}</div>
                <div>Status : {char.status}</div>
                <div>Nickname : {char.nickname}</div>
                <div>Portrayed : {char.portrayed}</div>
                <div>Appearance in season : {char.appearance}</div>
                <div>Quotes : {
                    loader ? <h3>Loading...</h3> : quotes.length ? quotes.map(quote => <div key={quote.quote_id} className="quotes">{quote.quote }</div>) : "No Quotes"
                }
                </div>
            </div>
            <Link to="/"><button className="btn btn-danger btn-lg">close</button></Link>
            
            
        </div>
    )
}

export default CharacterDetails
