import React,{useEffect,useState} from 'react'
import './characterCard.scss';


function CharacterCard({character}) {

    const [isModal,setIsModal] = useState(false);

    useEffect(()=>{
        console.log(character);
    })
    const handleCharacter = (char) => {
        setIsModal(true);
    }
    return (
        <div className="wrapper">
            <div className="character" onClick={handleCharacter} key={character.char_id}>
                 <img src={`${character.img}`} alt="name" className="characterimg" />

            </div>
            {
                // isModal && <div className="detailsWrapper"> <CharacterDetails details={character} isModal={isModal} setIsModal={setIsModal}/></div>
            }
        </div>
    )
}

export default CharacterCard
