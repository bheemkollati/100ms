import React,{useState} from 'react'

import './characters.scss';
import CharacterCard from './CharacterCard';
import { Link } from 'react-router-dom';

function Characters({characters,loading}) {
    const [search,setSearch]=useState("");

    const handleSearch = (e)=>{
        setSearch(e.target.value);
    }
   
   
    return (
        <div>
         
           <div>    
                <h1>Breaking Bad Characters</h1>
                <form><input type="text" className="form-control" value={search} placeholder="Search" onChange={handleSearch}/></form>
                {
                    
                (loading) ? <h1>Loading...</h1> :
                    <div className="charcards">
                    
                    {characters.filter((character) => {
                        if(character===" "){
                            return character
                        }else if(character.name.toLowerCase().includes(search.toLowerCase())){
                            return character
                        }
                    }).map(character => 
                        <Link to={`/characters/${character.char_id}`}><CharacterCard character={character}/></Link>
                    
                    )}
                
                </div>}
            </div>
       
        
        </div>
    )
}

export default Characters
