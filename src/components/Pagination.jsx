import React from 'react';
import './pagination.scss';

function Pagination({charactersperpage,totalcharacters,paginate}) {
    const pageNumbers=[];
    console.log(totalcharacters);

    for(let i=1;i<=Math.ceil(totalcharacters/charactersperpage);i++){
        pageNumbers.push(i);
    }

    console.log(pageNumbers);
    return (
        <nav>
            <ul className="pagination">
                {pageNumbers.map(number => (
                    <li key={number} className="page-item">
                        <a href="!#" className="page-link" onClick={() => paginate(number)}>
                            {number}
                        </a>
                    </li>
                ))}
            </ul>  
        </nav>
    )
}

export default Pagination
