import "./App.css";
import Pagination from "./components/Pagination";
import Characters from "./components/Characters";
import { useState, useEffect } from "react";
import axios from "axios";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CharacterDetails from "./components/CharacterDetails";
function App() {
  const [characters, setCharacters] = useState([]);
  const [loading, setLoading] = useState(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [charactersperpage] = useState(10);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  useEffect(() => {
    setLoading(true);
    axios
      .get("https://breakingbadapi.com/api/characters")
      .then((res) => {
        setCharacters(res.data);
        setLoading(false);
      })
      .catch((err) => console.log(err));
  }, []);

  const indexOfLastItem = currentPage * charactersperpage;
  const indexOfFirstItem = indexOfLastItem - charactersperpage;
  const currentcharacters = characters.slice(indexOfFirstItem, indexOfLastItem);

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/characters/:id" component={CharacterDetails}></Route>
          <Route path="/">
            {" "}
            <Characters characters={currentcharacters} loading={loading} />
            <Pagination
              charactersperpage={charactersperpage}
              totalcharacters={characters.length}
              paginate={paginate}
            />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
